<!--
`pandoc` is doing all the work here, using [style.css](style.css). It's run as part of the build process, see .gitlab-ci.yml. Basically anything that is to be published should end up in the "public" folder.

pandoc --css=style.css --standalone README.md -o public/index.html

The whole site is created from a single `README.md` file.

See the [repo](https://gitlab.com/deanturpin/markdown-with-css), the final website is hosted by GitLab here: [https://deanturpin.gitlab.io/markdown-with-css/](https://deanturpin.gitlab.io/markdown-with-css/). If you fork this repo then you will need to use your own username, see Settings > Pages in GitLab.
-->

# Markdown tutorial
Markdown is now my go-to format. It's not as expressive as HTML, say, but it's quick to type and very straightforward to generated programatically (you don't have to match tags).

So let's start with some structure.

## Headings
Declare a heading by prefixing up to five hashes. The hashes are equivalent to HTML `<h1>` heading tags, I like to use the single hash only once at the top.

```markdown
# h1
## h2
### h3
#### h4
##### h5
```

## Links
Next you'll want to link to another page. Let's credit [Pascal Hertleif](https://gist.github.com/killercup) for [this awesome gist](https://gist.github.com/killercup/5917178) by which the CSS for this site is based on.

It takes a while to remember which comes first: the square or the round brackets. In fact, you will never remember.
```markdown
[Pascal Hertleif](https://gist.github.com/killercup)
```

### Linking to images
You link to images in the same way but prefix a pling, note you can leave the leading brackets empty.

```markdown
![](https://ping.germs.dev/hosts.svg)
```

If you want to also link to that image it starts to look a little unweildy! But basically you're adding an image to the first set of brackets.
```markdown
[![](https://ping.germs.dev/hosts.svg)](https://ping.germs.dev/hosts.svg)
```

[![](https://ping.germs.dev/hosts.svg)](https://ping.germs.dev/hosts.svg)

## Preformatted text
Often used as "don't format this text!" and comes in span or block style. Use single or triple backticks around your code and you can specify the language too.

Using:
```
```cpp
```

```cpp
int main() {
    // cya
    return 0;
}
```

Using:
```
```python
```

```python
int main() {
    // cya
    return 0;
}
```


Or a single pair of back ticks around a word like `mkdir`.

## Mermaid
See mermaid.live, only works in GitLab but I've included it as I use it all the time and it's still valid markdown even if your viewer doesn't support it..

```mermaid
graph TD
    A[Christmas] -->|Get money| B(Go shopping)
    B --> C{Let me think}
    C -->|One| D[Laptop]
    C -->|Two| E[iPhone]
    C -->|Three| F[fa:fa-car Car]
```

You can insert an m-dash using two hyphens -- like this: `--` -- which is neater than using HTML entities. 

Similary for ellipses, you just type three periods and it inserts the proper character... which is nice.

## Paragraph breaks
A blank line makes a paragraph. If you really want a new line but don't want to use bullets you can add a double space at the end of the line. But this is difficult to see/manage.

## Bullet points
A leading hyphen makes a bullet point.
```
- once
- twice
- thrice
```
- one
- two
- three

Or use a number for an enumerated list. Note you don't have to increment the numbers yourself.

```
1. once
1. twice
1. thrice
```

1. one
1. two
1. three

## Tables
```
| Beats per second | Beats per hour |
|---|---|
| 3	| 10800 |
| 4	| 14400 |
| 5	| 18000 |
| 6	| 21600 |
| 7	| 25200 |
| 8	| 28800 |
```

Makes a nice table like this.

| Beats per second | Beats per hour |
|---|---|
| 3	| 10800 |
| 4	| 14400 |
| 5	| 18000 |
| 6	| 21600 |
| 7	| 25200 |
| 8	| 28800 |


## Horizontals rule
Three hyphens make a rule, akin to `<hr>`.
```
---
```
---

## References
- [Markdown on Wikipedia](https://en.wikipedia.org/wiki/Markdown)

## &#128337; Last updated
